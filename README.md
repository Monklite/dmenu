dmenu - dynamic menu
====================
dmenu is an efficient dynamic menu for X.

This fork contains the following patches:
- Highlight
  - This patch highlights the individual characters of matched text for each dmenu list entry. 
- Lineheight
  - The patch adds a '-h' option, which sets the minimum height of a dmenu line. 
- Numbers
  - Adds text which displays the number of matched and total items in the top right corner of dmenu. 

The colors match the OneDark colorscheme.

Requirements
------------
In order to build dmenu you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dmenu is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dmenu
(if necessary as root):

    make clean install


Running dmenu
-------------
See the man page for details.
